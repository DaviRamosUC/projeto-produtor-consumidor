package domain;

public class Buffer {

	private int conteudo;
	private boolean disponivel;

	public synchronized void set(int idProdutor) {
		while (disponivel == true) {
			try {
				System.out.println("Produtor #" + idProdutor + " esperando...");
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		conteudo++;
		System.out.println("Produtor #" + idProdutor + " colocou " + conteudo);
		disponivel = true;
		notifyAll();
	}

	public synchronized int get(int idConsumidor) {
		while (disponivel == false) {
			try {
				System.out.println("Consumidor #" + idConsumidor + " esperado...");
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Consumidor #" + idConsumidor + " consumiu: " + conteudo);
		conteudo--;
		disponivel = false;
		notifyAll();
		return conteudo;
	}
}
