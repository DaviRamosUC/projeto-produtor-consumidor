package domain;

public class Produtor extends Thread {
    private int idProdutor;
    private Buffer pilha;
    private int producaoTotal;
 
    public Produtor(int id, Buffer p, int producaoTotal) {
        idProdutor = id;
        pilha = p;
        this.producaoTotal = producaoTotal;
    }
 
    @Override
    public void run() {
        for (int i = 0; i < producaoTotal; i++) {
            pilha.set(idProdutor);
        }
        System.out.println("Produtor #" + idProdutor + " concluido!");
    }

	public int getProducaoTotal() {
		return producaoTotal;
	}

	public void setProducaoTotal(int producaoTotal) {
		this.producaoTotal = producaoTotal;
	}
    
    
}
