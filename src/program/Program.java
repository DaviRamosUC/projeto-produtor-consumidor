package program;

import domain.Buffer;
import domain.Consumidor;
import domain.Produtor;

public class Program {
	
	public static void main(String[] args) {
	    Buffer bufferCompartilhado = new Buffer();
	   
	    Produtor p1 = new Produtor(1, bufferCompartilhado, 5);
	    Produtor p2 = new Produtor(2, bufferCompartilhado, 5);
	  
	    Consumidor c1 = new Consumidor(1, bufferCompartilhado, 5);
	    Consumidor c2 = new Consumidor(2, bufferCompartilhado, 5);
	    
	    p1.start();
	    p2.start();  
	    c1.start();   
	    c2.start();
	    
	}
}
